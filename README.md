# README eindopdracht testen
Naam: David Peetoom  
Studentnummer: S1120019

## Deel 1 Unittests

### 1.1 PremiumCalculationTest

#### 1.1.1 TheBasePremiumCanBeCalculatedCorrectly

##### Wat is het doel van de unittest?
- Testen of de basispremie correct berekend kan worden.

##### Waarom is er voor deze specifieke invulling van data van de unittest gekozen?
- Als waarden zijn er normale waarden genomen voor het berekenen van een basispremie. (Deze waarden zijn ook gebruikt als basispremie voor de rest van de testen)
- 70 KW
- 5000 Euro
- 1 Jaar oude auto
- 21 Is de verwachte basispremie want: ( 5000 / 100 - 1 + 70 / 5 ) / 3 = 21

##### Welke technieken zijn er gebruikt om tot deze data te komen?
- Hier is een Fact gebruikt omdat het één simpele berekening bevat.

#### 1.1.2 FifteenPercentExtraFeeIfAgeIsUnderTwentyThreeOrLicenseIsUnderFiveYearsOld

##### Wat is het doel van de unittest?
- Testen of de vijftien procent extra premie correct bij de basispremie opgeteld wordt als een persoon 23 jaar en ouder is of de persoon langer dan 5 jaar zijn rijbewijs heeft

##### Waarom is er voor deze specifieke invulling van data van de unittest gekozen?
- De variabele waarden zijn Leeftijd van de persoon en het aantal jaren dat de persoon zijn rijbewijs heeft.
- 22 jaar en 6 jaar voor het testen van de leeftijd van de persoon om grenswaarde 23 heen.
- 23 jaar en 6 jaar voor het testen van de leeftijd van de persoon om grenswaarde 23 heen.
- 24 jaar en 6 jaar voor het testen van de leeftijd van de persoon om grenswaarde 23 heen.
- 30 jaar en 4 jaar voor het testen van het aantal jaren dat de persoon zijn rijbewijs heeft om grenswaarde 5 heen.
- 30 jaar en 5 jaar voor het testen van het aantal jaren dat de persoon zijn rijbewijs heeft om grenswaarde 5 heen.
- 30 jaar en 6 jaar voor het testen van het aantal jaren dat de persoon zijn rijbewijs heeft om grenswaarde 5 heen.

##### Welke technieken zijn er gebruikt om tot deze data te komen?
- Hier is een Theory gebruikt voor het testen van 6 verschillende data variaties.

#### 1.1.3 UpdatePremiumWithDifferentPostalCodesWithoutAnyOtherFeesWorksCorrectly

##### Wat is het doel van de unittest?
- Testen of het updaten van de premie correct werkt bij het optellen van de premie bij het hebben van verschillende postcodes om de grenswaarden heen.

##### Waarom is er voor deze specifieke invulling van data van de unittest gekozen?
- De variabele waarde is de postcode van de persoon.
- Postcode 1000 voor het testen van grenswaarde 1000. (999 niet want er moet minimaal 1000 ingevuld worden)
- Postcode 3599 voor het testen van grenswaarde 3600.
- Postcode 3600 voor het testen van grenswaarde 3600.
- Postcode 4499 voor het testen van grenswaarde 4500.
- Postcode 4500 voor het testen van grenswaarde 4500.

##### Welke technieken zijn er gebruikt om tot deze data te komen?
- Hier is een Theory gebruikt voor het testen van 5 verschillende data variaties.

#### 1.1.4 PremiumAmountIsDifferentForDifferentInsuranceCoverages

##### Wat is het doel van de unittest?
- Testen of de premie correct berekend wordt met het gebruikmaken van verschillende soorten verzekering dekkingen.

##### Waarom is er voor deze specifieke invulling van data van de unittest gekozen?
- De variabele waarde is de soort dekking van de verzekering.
- WA
- WA_PLUS
- ALL_RISK
- Deze 3 zijn gebruikt omdat dit alle soorten dekkingen zijn die er te gebruiken zijn.

##### Welke technieken zijn er gebruikt om tot deze data te komen?
- Hier is een Theory gebruikt voor het testen van 3 verschillende data variaties.

#### 1.1.5 PremiumAmountIsCalculatedCorrectlyWithDifferentDamageFreeYears 

##### Wat is het doel van de unittest?
- Testen of de premie correct berekend wordt bij het hebben van verschillende schadevrije jaren om de grenswaarden heen.

##### Waarom is er voor deze specifieke invulling van data van de unittest gekozen?
- De variabele waarde is het aantal schadevrije jaren van de persoon.
- 5 jaar voor het testen van het ontbreken van korting.
- 6 jaar voor het testen van het eerste kortingspercentage van 5 %.
- 17 jaar voor het testen van het hebben van de waarde onder de maximale korting van 60 %.
- 18 jaar voor het testen van het hebben van de maximale korting van 65 %.
- 19 jaar voor het testen van het hebben van de maximale korting, en om te kijken of hij niet over die 65 % heen gaat.

##### Welke technieken zijn er gebruikt om tot deze data te komen?
- Hier is een Theory gebruikt voor het testen van 5 verschillende data variaties.

#### 1.1.6 PremiumAmountIsCorrectlyCalculatedWithDifferentTermsOfPayment

##### Wat is het doel van de unittest?
- Testen of de berekening van de uiteindelijke premie goed verloopt bij het hebben van verschillende betalingstermijnen.

##### Waarom is er voor deze specifieke invulling van data van de unittest gekozen?
- De variabele waarde is de betalingstermijn van de betaling van de premie.
- YEAR ( 2.5 % korting )
- MONTH
- Deze 2 zijn gebruikt omdat dit alle te kiezen betalingstermijnen zijn.

##### Welke technieken zijn er gebruikt om tot deze data te komen?
- Hier is een Theory gebruikt voor het testen van 2 verschillende data variaties.

#### 1.1.7 FinalPremiumIsCorrectlyRoundedToTwoDecimals

##### Wat is het doel van de unittest?
- Testen of de uiteindelijke afronding van de premie, naar 2 getallen achter de komma, goed gedaan wordt.

##### Waarom is er voor deze specifieke invulling van data van de unittest gekozen?
- De variabele waarde is de uiteindelijk te betalen premie.
- 66 is gekozen, omdat 66 / 12 = 5.5 , Hierdoor kan goed gezien worden of de getallen goed afgerond worden naar 2 getallen achter de komma. Namelijk 5.50.
- 72 is gekozen, omdat 72 / 12 = 6 , Hierdoor kan goed gezien worden of de getallen goed afgerond worden naar 2 getallen achter de komma. Namelijk 6.00.

##### Welke technieken zijn er gebruikt om tot deze data te komen?
- Hier is een Mock gebruikt voor het simuleren van de premie zodat het op een heel getal uitkomt, zodat de afronding goed getest kan worden.

### 1.2 PolicyHolderTest

#### 1.2.1 CanCalculateLicenseAgeByYears

##### Wat is het doel van de unittest?
- Kijken of de berekening van het aantal jaren dat de persoon zijn rijbewijs heeft, correct gaat.

##### Waarom is er voor deze specifieke invulling van data van de unittest gekozen?
- 5 jaar en 2 maanden om te kijken of de berekening uberhaupt goed gaat.
- 6 jaar en 2 maanden om te kijken of de berekening goed gaat met verschillende jaren.
- 4 jaar en 10 maanden om te kijken of de berekening goed gaat met verschillende maanden.
- 5 jaar en 10 maanden om te kijken of de berekening goed gaat met verschillende maanden en jaren.
- 0 jaar en 2 maanden zodat het nog in hetzelfde jaar valt en het aantal jaar op 0 uitkomt.
- 0 jaar en 0 maanden zodat het ook werkt op de dag van vandaag.

##### Welke technieken zijn er gebruikt om tot deze data te komen?
- Hierbij is gebruik gemaakt van een Theory omdat het een redelijk simpele test is met 6 verschillende data variaties.
- In de Arrange is de driverLicenseStartDate berekend en omgezet naar een string door het aantal jaren en maanden af te trekken van de datum op dit moment.

### 1.3 VehicleTest

#### 1.3.1 CanCalculateAgeOfVehicleInYears

##### Wat is het doel van de unittest?
- Kijken of de leeftijd van de voertuig correct berekend wordt.

##### Waarom is er voor deze specifieke invulling van data van de unittest gekozen?
- 2020 om te kijken of het goed gaat als je een afgelopen jaar invult.
- 2021 om te kijken of het goed gaat als je het huidige jaar invult.
- 2022 om te kijken of het goed gaat als je een volgend jaar invult.

##### Welke technieken zijn er gebruikt om tot deze data te komen?
- Hierbij is gebruik gemaakt van een Theory omdat het een simpele test is met 3 verschillende data variaties.

## Deel 2 Fouten applicatie

##### Wat heb je getest?
- De hele applicatie een aantal keer doorgelopen met verschillende waardes die de requirements testen.
- Er zijn unittesten geschreven en hierdoor zijn er fouten uit de applicatie gehaald.

#### 2.1 Testgeval zonder extra kosten naast basispremie

##### Wat had je verwacht?
- 4.52 per maand, maar in de console verschijnt 4.5 per maand.

##### Waar komt deze verwachting vandaan?
- De basispremie per jaar is: (Waarde voertuig / 100 - leeftijd + vermogen in KW / 5) / 3
- Dezelfde formule voor de berekening van de basispremie wordt aangehouden als in de applicatie gebruikt wordt: 5000/100 - 1 + 80 / 5 / 3 = 54.33
- Leeftijd is 25 jaar en persoon heeft 6 jaar zijn rijbewijs (28-04-2015).
- Postcode is 4800.
- WA standaard gekozen.
- 0 schadevrije jaren.
- Betaling per maand.
- 54.33 / 12 = 4.52 per maand.

##### Wat gaat er mis?
- Er gaat iets mis met de afronding van het uiteindelijke getal (4.5 maar moet 4.50 zijn)
- Er gaat iets mis in de berekening (met haakjes): 5000/100 - 1 + 80 / 5 / 3 = 54.33 en (5000/100 - 1 + 80 / 5) / 3 = 21.66

#### 2.2 Testgeval met extra kosten naast basispremie

##### Wat had je verwacht?
- 6.56 per maand, maar in de console verschijnt 6.52 per maand.

##### Waar komt deze verwachting vandaan?
- De basispremie per jaar is: (Waarde voertuig / 100 - leeftijd + vermogen in KW / 5) / 3
- Dezelfde basispremie wordt aangehouden: 5000/100 - 1 + 80 / 5 / 3 = 54.33
- Leeftijd is 21 jaar dus 15 % opslag.
- Postcode is 1273 dus 5 % opslag.
- WA plus is gekozen dus 20 % duurder.
- 0 schadevrije jaren.
- Betaling per maand.
- (54.33 * 1.15 * 1.05 * 1.20) / 12 = 6.56 per maand.

##### Wat gaat er mis?
- Er gaat iets mis in de afronding in de CalculateBasePremium functie. (54.33 wordt afgerond naar 54. In het volgende testgeval wordt daarom de waarde 54 als basispremie aangehouden.)

#### 2.3 Testgeval zonder extra kosten naast basispremie maar met 10 schadevrije jaren

##### Wat had je verwacht?
- 3.38 per maand, maar in de console verschijnt 0 per maand.

##### Waar komt deze verwachting vandaan?
- De basispremie per jaar is: (Waarde voertuig / 100 - leeftijd + vermogen in KW / 5) / 3
- Dezelfde basispremie wordt aangehouden: 5000/100 - 1 + 80 / 5 / 3 = 54
- Leeftijd is 25 jaar en persoon heeft 6 jaar zijn rijbewijs (28-04-2015).
- Postcode is 4800.
- WA standaard gekozen.
- 10 schadevrije jaren, 25 % korting dus.
- Betaling per maand.
- 54 * 0.75 / 12 = 3.38 per maand.

##### Wat gaat er mis?
- Er gaat iets mis in de UpdatePremiumForNoClaimYears functie.

#### 2.4 Conclusie
- Er gaat iets mis met de afronding van het uiteindelijke getal.
- Er gaat iets mis in de afronding in de CalculateBasePremium functie.
- Er gaat iets mis in de UpdatePremiumForNoClaimYears functie.

## Deel 3 Oplevering
- Link naar GitLab repository: https://gitlab.com/Davidpeetoom/CarInsuranceCalculator

## Deel 4 Verbeteren Code

#### 4.1 CalculateBasePremium functie afronding fouten
- Line 70 in PremiumCalculation.cs ```return vehicle.ValueInEuros / 100 - vehicle.Age + vehicle.PowerInKw / 5 / 3;``` veranderen naar ```return (vehicle.ValueInEuros / 100.0 - vehicle.Age + vehicle.PowerInKw / 5.0) / 3.0;```
- Hierdoor rond hij niet meer af naar een heel getal.
- Er zijn ook gelijk haakjes om de berekening gezet, anders gaat de berekening fout.

#### 4.2 Afronding uiteindelijke premie fout oplossen
- Er is een fout van de berekening van de korting bij het betalen van jaren.
- Line 64 in PremiumCalculation.cs ```double result = period == PaymentPeriod.YEAR ? PremiumAmountPerYear / 1.025 : PremiumAmountPerYear / 12;``` veranderen naar ```double result = period == PaymentPeriod.YEAR ? PremiumAmountPerYear * 0.975 : PremiumAmountPerYear / 12;```

#### 4.3 UpdatePremiumForNoClaimYears fout oplossen
- Alle getallen groter dan 5 jaar, maken van de premium 0.
- Line 52 in PremiumCalculation.cs ```return premium * ((100 - NoClaimPercentage) / 100);``` veranderen naar ```return premium * ((100.0 - NoClaimPercentage) / 100.0);```
- Uiteindelijk bleek dit een afrondingsfout te zijn.

#### 4.4 Uiteindelijke bedrag laat geen nullen zien, dus 45.1 ipv 45.10
- Line 118 in Program.cs ```Console.WriteLine("Your premium is {0} per {1}", calculation.PremiumPaymentAmount(p), timeunit);``` veranderen naar ```Console.WriteLine("Your premium is {0} per {1}", calculation.PremiumPaymentAmount(p).ToString("0.00"), timeunit);```
- Om het om te zetten naar een string met 2 getallen achter de komma.

#### 4.5 Fout in berekening voor schadevrije jaren
- Lines 50 en 51 in PremiumCalculation.cs ```if (NoClaimPercentage > 65) { NoClaimPercentage = 65; } if (NoClaimPercentage < 0) { NoClaimPercentage = 0; }``` veranderen naar ```if (NoClaimPercentage > 60) { NoClaimPercentage = 65; } if (NoClaimPercentage < 5) { NoClaimPercentage = 0; }```

### Deel 5 Extra
- Uitvoeren van Stryker in de Implementation folder levert geen ongedekte mutanten op.
