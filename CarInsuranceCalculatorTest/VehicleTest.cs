using System;
using Xunit;
using WindesheimAD2021AutoVerzekeringsPremie.Implementation;

namespace CarSuranceCalculatorTest
{
    public class VehicleTest
    {

        [Theory]
        [InlineData(-1, 1)]
        [InlineData(0, 0)]
        [InlineData(1, 0)]
        public void CanCalculateAgeOfVehicleInYears(int year, int expectedAge)
        {
            //Arrange
            int constructionYear = DateTime.Now.Year + year;

            //Act
            var vehicle = new Vehicle(60, 5000, constructionYear);

            //Assert
            Assert.Equal(expectedAge, vehicle.Age);
        }
    }
}