using System;
using Xunit;
using WindesheimAD2021AutoVerzekeringsPremie.Implementation;
using Moq;

namespace CarSuranceCalculatorTest
{
    public class PremiumCalculationTest
    {
        [Fact]
        public void TheBasePremiumCanBeCalculatedCorrectly()
        {
            //Arrange
            int powerInKw = 70;
            int valueInEuros = 5000;
            int constructionYear = DateTime.Now.Year - 1;

            Vehicle vehicle = new Vehicle(powerInKw, valueInEuros, constructionYear);

            double expectedBasePremium = 21;
            
            //Act
            double actualBasePremium = PremiumCalculation.CalculateBasePremium(vehicle);

            //Assert
            Assert.Equal(expectedBasePremium, actualBasePremium);
        }

        [Theory]
        [InlineData(22, 6, 21 * 1.15)]
        [InlineData(23, 6, 21)]
        [InlineData(24, 6, 21)]
        [InlineData(30, 4, 21 * 1.15)]
        [InlineData(30, 5, 21 * 1.15)]
        [InlineData(30, 6, 21)]

        public void FifteenPercentExtraFeeIfAgeIsUnderTwentyThreeOrLicenseIsUnderFiveYearsOld(int age, int licenseAge, double expectedPremium)
        {
            //Arrange
            Vehicle vehicle = new Vehicle(70, 5000, 2020);

            string licenseDate = DateTime.Now.AddYears(-licenseAge).ToString("dd-MM-yyyy");
            PolicyHolder policyHolder = new PolicyHolder(age, licenseDate, 4500, 0);

            InsuranceCoverage coverage = InsuranceCoverage.WA;

            //Act
            PremiumCalculation actualPremium = new PremiumCalculation(vehicle, policyHolder, coverage);

            //Assert
            Assert.Equal(expectedPremium, actualPremium.PremiumAmountPerYear);
        }

        [Theory]
        [InlineData(1000, 21 * 1.05)]
        [InlineData(3599 , 21 * 1.05)]
        [InlineData(3600 , 21 * 1.02)]
        [InlineData(4499 , 21 * 1.02)]
        [InlineData(4500 , 21)]
        public void UpdatePremiumWithDifferentPostalCodesWithoutAnyOtherFeesWorksCorrectly(int postalCode, double expectedPremium)
        {
            //Arrange
            Vehicle vehicle = new Vehicle(70, 5000, 2020);
            PolicyHolder policyHolder = new PolicyHolder(30, DateTime.Now.AddYears(-6).ToString("dd-MM-yyyy"), postalCode, 0);
            InsuranceCoverage coverage = InsuranceCoverage.WA;

            //Act
            PremiumCalculation actualPremium = new PremiumCalculation(vehicle, policyHolder, coverage);

            //Assert
            Assert.Equal(expectedPremium, actualPremium.PremiumAmountPerYear);
        }

        [Theory]
        [InlineData(InsuranceCoverage.WA, 21)]
        [InlineData(InsuranceCoverage.WA_PLUS, 21 * 1.20)]
        [InlineData(InsuranceCoverage.ALL_RISK, 21 * 2)]
        static void PremiumAmountIsDifferentForDifferentInsuranceCoverages(InsuranceCoverage coverage, double expectedPremium)
        {
            //Arrange
            Vehicle vehicle = new Vehicle(70, 5000, 2020);
            PolicyHolder policyHolder = new PolicyHolder(30, DateTime.Now.AddYears(-6).ToString("dd-MM-yyyy"), 4500, 0);

            //Act
            PremiumCalculation actualPremium = new PremiumCalculation(vehicle, policyHolder, coverage);

            //Assert
            Assert.Equal(expectedPremium, actualPremium.PremiumAmountPerYear);
        }

        [Theory]
        [InlineData(5, 21)]
        [InlineData(6, 21 * 0.95)]
        [InlineData(17, 21 * 0.40)]
        [InlineData(18, 21 * 0.35)]
        [InlineData(19, 21 * 0.35)]
        public void PremiumAmountIsCalculatedCorrectlyWithDifferentDamageFreeYears(int damageFreeYears, double expectedPremium)
        {
            //Arrange
            Vehicle vehicle = new Vehicle(70, 5000, 2020);
            PolicyHolder policyHolder = new PolicyHolder(30, DateTime.Now.AddYears(-6).ToString("dd-MM-yyyy"), 4500, damageFreeYears);
            InsuranceCoverage coverage = InsuranceCoverage.WA;

            //Act
            PremiumCalculation actualPremium = new PremiumCalculation(vehicle, policyHolder, coverage);

            //Assert
            Assert.Equal(expectedPremium, actualPremium.PremiumAmountPerYear);
        }

        [Theory]
        [InlineData(PremiumCalculation.PaymentPeriod.YEAR, 21 * 0.975)]
        [InlineData(PremiumCalculation.PaymentPeriod.MONTH, 21 / 12.0)]
        static void PremiumAmountIsCorrectlyCalculatedWithDifferentTermsOfPayment(PremiumCalculation.PaymentPeriod period, double expectedResult)
        {
            //Arrange
            Vehicle vehicle = new Vehicle(70, 5000, 2020);
            PolicyHolder policyHolder = new PolicyHolder(30, DateTime.Now.AddYears(-6).ToString("dd-MM-yyyy"), 4500, 0);
            InsuranceCoverage coverage = InsuranceCoverage.WA;

            //Act
            PremiumCalculation actualPremium = new PremiumCalculation(vehicle, policyHolder, coverage);
            double ActualResult = actualPremium.PremiumPaymentAmount(period);

            //Assert
            Assert.Equal(Math.Round(expectedResult, 2), ActualResult);
        }

        [Fact]
        public void FinalPremiumIsCorrectlyRoundedToTwoDecimals()
        {
            //Arrange
            Vehicle vehicle = new Vehicle(70, 5000, 2020);
            PolicyHolder policyHolder = new PolicyHolder(30, DateTime.Now.AddYears(-6).ToString("dd-MM-yyyy"), 4500, 0);
            InsuranceCoverage coverage = InsuranceCoverage.WA;

            //Act
            Mock<PremiumCalculation> mockActualPremium = new Mock<PremiumCalculation>(vehicle, policyHolder, coverage);
            mockActualPremium.SetupSequence(premium => premium.PremiumAmountPerYear).Returns(66).Returns(72);

            double actualResult1 = mockActualPremium.Object.PremiumPaymentAmount(PremiumCalculation.PaymentPeriod.MONTH);
            double actualResult2 = mockActualPremium.Object.PremiumPaymentAmount(PremiumCalculation.PaymentPeriod.MONTH);

            //Assert
            Assert.StrictEqual(5.50, actualResult1);
            Assert.StrictEqual(6.00, actualResult2);
        }
    }
}
