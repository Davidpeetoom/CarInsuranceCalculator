using System;
using Xunit;
using WindesheimAD2021AutoVerzekeringsPremie.Implementation;

namespace CarSuranceCalculatorTest
{
    public class PolicyHolderTest
    {
        [Theory]
        [InlineData(5, 2, 5)]
        [InlineData(6, 2, 6)]
        [InlineData(4, 10, 4)]
        [InlineData(5, 10, 5)]
        [InlineData(0, 2, 0)]
        [InlineData(0, 0, 0)]
        public void CanCalculateLicenseAgeByYears(int licenseAgeYears, int licenseAgeMonths, int expectedAge)
        {
            //Arrange
            string driverLicenseStartDate = DateTime.Now.AddYears(-licenseAgeYears).AddMonths(-licenseAgeMonths).ToString("dd-MM-yyyy");

            //Act
            var policyHolder = new PolicyHolder(21, driverLicenseStartDate, 4500, 0);

            //Assert
            Assert.Equal(expectedAge, policyHolder.LicenseAge);
        }
    }
}